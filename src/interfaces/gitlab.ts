export interface GitLabTag {
  commit: {
    id: string
    short_id: string
    title: string
    created_at: string
    parent_ids?: string[]
    message: string
    author_name: string
    author_email: string
    authored_date: string
    committer_name: string
    committer_email: string
    committed_date: string
  }
  release: {
    tag_name: string
    description: string
  }
  name: string
  target: string
  message?: null
}

export interface GitLabJob {
  commit: {
    author_email: string
    author_name: string
    created_at: Date
    id: string
    message: string
    short_id: string
    title: string
  }
  coverage?: any
  created_at: Date
  started_at: Date
  finished_at: Date
  duration: number
  artifacts_expire_at: Date
  id: number
  name: string
  pipeline: {
    id: number
    ref: string
    sha: string
    status: string
  }
  ref: string
  artifacts: any[]
  runner?: any
  stage: string
  status: string
  tag: boolean
  web_url: string
  user: {
    id: number
    name: string
    username: string
    state: string
    avatar_url: string
    web_url: string
    created_at: Date
    bio?: any
    location?: any
    public_email: string
    skype: string
    linkedin: string
    twitter: string
    website_url: string
    organization: string
  }
}

export interface GitLabGroup {
  id: number
  description?: any
  default_branch: string
  ssh_url_to_repo: string
  http_url_to_repo: string
  web_url: string
  readme_url: string
  tag_list: string[]
  name: string
  name_with_namespace: string
  path: string
  path_with_namespace: string
  created_at: Date
  last_activity_at: Date
  forks_count: number
  avatar_url: string
  star_count: number
}

export interface GitLabProject {
  id: number
  description: string
  default_branch: string
  tag_list: any[]
  archived: boolean
  visibility: string
  ssh_url_to_repo: string
  http_url_to_repo: string
  web_url: string
  name: string
  name_with_namespace: string
  path: string
  path_with_namespace: string
  issues_enabled: boolean
  merge_requests_enabled: boolean
  wiki_enabled: boolean
  jobs_enabled: boolean
  snippets_enabled: boolean
  created_at: Date
  last_activity_at: Date
  shared_runners_enabled: boolean
  creator_id: number
  namespace: {
    id: number
    name: string
    path: string
    kind: string
  }
  avatar_url?: any
  star_count: number
  forks_count: number
  open_issues_count: number
  public_jobs: boolean
  shared_with_groups: any[]
  request_access_enabled: boolean
}

export interface GitLabPipeline {
  id: number
  status: string
  ref: string
  sha: string
  web_url: string
}

export interface GitLabPipelineDetails {
  id: number
  sha: string
  ref: string
  status: string
  before_sha: string
  tag: boolean
  yaml_errors?: any
  user: {
    name: string
    username: string
    id: number
    state: string
    avatar_url: string
    web_url: string
  }
  created_at: Date
  updated_at: Date
  started_at?: any
  finished_at?: any
  committed_at?: any
  duration?: any
  coverage?: any
  web_url: string
}
