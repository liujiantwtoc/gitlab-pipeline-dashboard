import axios, { AxiosRequestConfig, AxiosInstance } from 'axios'
import {
  GitLabGroup,
  GitLabJob,
  GitLabPipeline,
  GitLabProject,
  GitLabPipelineDetails,
  GitLabTag
} from '../interfaces/gitlab'

class GitLab {
  public client: AxiosInstance
  public apiBase = '/api/v4'

  constructor(url: string, token: string, clientConfig?: AxiosRequestConfig) {
    this.client = axios.create({
      baseURL: url,
      headers: {
        'Private-Token': token,
      },
      timeout: 10 * 1000,
      ...clientConfig,
    })
  }

  public async searchGroups(search: string, options?: any): Promise<GitLabGroup[]> {
    return this.get('/groups', { search, ...options })
  }

  public async getProjects(groupId: number, options?: any): Promise<GitLabProject[]> {
    return this.get(`/projects/${groupId}`, { ...options })
  }

  public async searchProjects(search: string, options?: any): Promise<GitLabProject[]> {
    return this.get(`/projects`, { search, ...options })
  }

  public async getAllProjects(groupId: number, options?: any): Promise<GitLabProject[]> {
    let projects = [] as GitLabProject[]
    let page = 1
    const perPage = 100
    while (page === 1 || projects.length === perPage) {
      const result = await this.get(`/groups/${groupId}/projects`, { ...options, per_page: perPage, page })
      projects = projects.concat(result)
      page++
    }
    return projects
  }

  public async getPipelines(projectId: number, options?: any): Promise<GitLabPipeline[]> {
    return this.get(`/projects/${projectId}/pipelines`, { ...options })
  }

  public async getPipelineDetails(projectId: number, pipelineId: number, options?: any)
    : Promise<GitLabPipelineDetails> {
    return this.get(`/projects/${projectId}/pipelines/${pipelineId}`, { ...options })
  }

  public async getProjectTags(projectId: number, options?: any): Promise<GitLabTag[]> {
    return this.get(`/projects/${projectId}/repository/tags`, { ...options })
  }

  public async getJobs(projectId: number, options?: any): Promise<GitLabJob> {
    return await this.get(`/projects/${projectId}/jobs`, { ...options })
  }

  private async get(url: string, params?: any) {
    const { data } = await this.client.get(`${this.apiBase}${url}`, { params })
    return data
  }
}

export default GitLab

