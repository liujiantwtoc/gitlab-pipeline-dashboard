#!/usr/bin/env node

const handler = require('serve-handler')
const http = require('http')
const path = require('path')
 
const server = http.createServer((request, response) => {
  return handler(request, response, {
    public: path.join(__dirname, './dist'),
    directoryListing: false,
  })
})

const PORT = process.env.PORT || 8080
 
server.listen(PORT, () => {
  console.log(`Gitlab Pipeline Dashboard is running at http://localhost:${PORT}`)
})
